﻿import React,{ Component} from 'react';
import {View, Text, TouchableOpacity, Image, Alert} from 'react-native';

var ImagePicker = require('react-native-image-picker');

// More info on all the options is below in the README...just some common use cases shown here
var options = {
  title: 'Select Avatar',
  customButtons: [
    {name: 'fb', title: 'Choose Photo from Facebook'},
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      avatarSource: null,
      data: null
    };
  }
  render(){
    let img = this.state.avatarSource == null ? null :
      <Image source={this.state.avatarSource}
            style={{height:100, width:100}}/>
    return(
      <View>
        <TouchableOpacity onPress={this.ShowImgPicker.bind(this)}>
          <Text>Show image picker</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.uploads.bind(this)}>
          <Text>Upload</Text>
        </TouchableOpacity>
        {img}
      </View>
    )
  }
  ShowImgPicker(){
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          data: response.data
        });
      }
    });
  }
  uploads(){
    if(this.state.data){
      fetch('http://192.168.111.2/upload/form.php', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "data": this.state.data
        })
      })
      .then(() => {
        Alert.alert(
           'Thông báo',
           'Tải ảnh lên thành công!!!',
           [
               { text: 'OK'}
           ],
           { cancelable: false }
       );
      })
      .catch(err =>{
        Alert.alert(
           'Thông báo',
           'Lỗi: '+err,
           [
               { text: 'OK'}
           ],
           { cancelable: false }
       );
     });
    }else{
      Alert.alert(
         'Thông báo',
         'Vui lòng chọn ảnh!',
         [
             { text: 'OK'}
         ],
         { cancelable: false }
     );
    }
  }
}
